/**
 * 
 */
package ex3;

// TODO: Auto-generated Javadoc
/**
 * The Class BlockingPlayer.
 *
 * @author arebe
 */
public class BlockingPlayer extends RandomPlayer {

	/**
	 * Instantiates a new blocking player.
	 *
	 * @param n
	 *            the n
	 * @param m
	 *            the m
	 */
	public BlockingPlayer(String n, char m) {
		super(n, m);
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ex3.Player#makeMove()
	 */
	public void makeMove() {

		int col = 0, row = 0; // it bugs me about initializing otherwise
		boolean needBlocking = false;

		outerloop: for (row = 0; row <= 2; row++) {
			for (col = 0; col <= 2; col++) {
				if (testForBlocking(row, col)) {
					System.out.println("testForBlocking Found a spot at row: " + row + " and at col: " + col);
					needBlocking = true;
					break outerloop; // breaks both for loops and row and col are correct
					// trust me on this one
				}
			}
		}

		if (needBlocking) {
			board.addMark(row, col, mark);
		} else {
			super.makeMove();
		}

	}

	/**
	 * Test for blocking.
	 *
	 * @param row
	 *            the row
	 * @param col
	 *            the col
	 * @return true, if board can be blocked at that spot
	 */
	public boolean testForBlocking(int row, int col) {
		char currentMark = board.getMark(row, col);
		if (currentMark != ' ') {
			return false;
		}
		// CHECK HORIZONTAL AND VERTICAL
		// Horizontal's done, unless it doesn't work.
		// SHIT IT DOESNT WORK
		// It's also inefficient in terms of checking, but screw it :P
		// for (int i = 0; i < 3; i++) { // i is row j is col
		// for (int j = 0; j < 3; j++) {
		/*
		 * if (2 - i - 1 >= 0) { if (board.getMark(2 - i, j) == board.getMark(2 - i - 1,
		 * j) && board.getMark(2 - i, j) == opponent.mark) { // check vertical if (i ==
		 * row && j == col) return true; } } if (2 - j - 1 >= 0) { if (board.getMark(i,
		 * 2 - j) == board.getMark(i, 2 - j - 1) && board.getMark(i, 2 - i) ==
		 * opponent.mark) { // check horizontal if (i == row && j == col) return true; }
		 * }
		 */

		// TRYING TO CODE DIAGONAL
		// if (i + j % 2 == 0) { // true if on a diagonal
		// CODE FOR DIAGONAL
		// ok... I give up. I'm doing cases for now, plz don't judge TOOO hard
		// if there's a way to do it non-painfully, then :P
		// LIKE SERIOUSLY TELL ME THX
		//
		// TOP ROW
		if (row == 0 && col == 0) { // check top left
			if (board.getMark(1, 1) == board.getMark(2, 2) && board.getMark(1, 1) == opponent.mark)// diagonal
				return true;
			else if (board.getMark(0, 1) == board.getMark(0, 2) && board.getMark(0, 1) == opponent.mark)// horizontal
				return true;
			else if (board.getMark(1, 0) == board.getMark(2, 0) && board.getMark(1, 0) == opponent.mark)// vertical
				return true;

		} else if (row == 0 && col == 1) { // check top middle
			if (board.getMark(0, 0) == board.getMark(0, 2) && board.getMark(0, 0) == opponent.mark)// horizontal
				return true;
			else if (board.getMark(1, 1) == board.getMark(2, 1) && board.getMark(1, 1) == opponent.mark)// vertical
				return true;

		} else if (row == 0 && col == 2) { // check top right
			if (board.getMark(1, 1) == board.getMark(2, 0) && board.getMark(1, 1) == opponent.mark)// diagonal
				return true;
			else if (board.getMark(0, 0) == board.getMark(0, 1) && board.getMark(0, 0) == opponent.mark)// horizontal
				return true;
			else if (board.getMark(1, 2) == board.getMark(2, 2) && board.getMark(1, 2) == opponent.mark)// vertical
				return true;
		}
		//
		// BOTTOM ROW
		else if (row == 2 && col == 0) { // check bottom right
			if (board.getMark(1, 1) == board.getMark(0, 2) && board.getMark(1, 1) == opponent.mark)// diagonal
				return true;
			else if (board.getMark(0, 0) == board.getMark(1, 0) && board.getMark(0, 0) == opponent.mark)// vertical
				return true;
			else if (board.getMark(2, 1) == board.getMark(2, 2) && board.getMark(2, 1) == opponent.mark)// horizontal
				return true;

		} else if (row == 2 && col == 1) { // check bottom middle
			if (board.getMark(2, 0) == board.getMark(2, 2) && board.getMark(2, 0) == opponent.mark)// horizontal
				return true;
			else if (board.getMark(1, 1) == board.getMark(0, 1) && board.getMark(1, 1) == opponent.mark)// vertical
				return true;

		} else if (row == 2 && col == 2) { // check bottom right
			if (board.getMark(0, 0) == board.getMark(1, 1) && board.getMark(1, 1) == opponent.mark)// diagonal
				return true;
			else if (board.getMark(0, 2) == board.getMark(1, 2) && board.getMark(0, 2) == opponent.mark)// vertical
				return true;
			else if (board.getMark(2, 0) == board.getMark(2, 1) && board.getMark(2, 0) == opponent.mark)// horizontal
				return true;

		}
		//
		// MIDDLE ROW
		else if (row == 1 && col == 1) { // check middle
			if ((board.getMark(0, 0) == board.getMark(2, 2) && board.getMark(0, 0) == opponent.mark)
					|| (board.getMark(0, 2) == board.getMark(2, 0) && board.getMark(0, 2) == opponent.mark))// diagonal
				return true;
			else if (board.getMark(0, 1) == board.getMark(2, 1) && board.getMark(0, 1) == opponent.mark)// vertical
				return true;
			else if (board.getMark(1, 0) == board.getMark(1, 2) && board.getMark(1, 0) == opponent.mark)// horizontal
				return true;

		} else if (row == 1 && col == 0) { // check middle left
			if (board.getMark(1, 1) == board.getMark(1, 2) && board.getMark(1, 1) == opponent.mark)// horizontal
				return true;
			else if (board.getMark(0, 0) == board.getMark(2, 0) && board.getMark(0, 0) == opponent.mark)// vertical
				return true;

		} else if (row == 1 && col == 2) { // check middle right
			if (board.getMark(1, 0) == board.getMark(1, 1) && board.getMark(1, 0) == opponent.mark)// horizontal
				return true;
			else if (board.getMark(0, 2) == board.getMark(2, 2) && board.getMark(0, 2) == opponent.mark)// vertical
				return true;

		}

		// }

		// }
		// }

		/*
		 * int traverseRow = 0; int traverseCol = 0; while (traverseRow < 3) { while
		 * (traverseCol < 3) { if (traverseRow != row || traverseCol != col) { char
		 * traverseMark = board.getMark(traverseRow, traverseCol); if (traverseMark !=
		 * mark) {
		 * 
		 * } } traverseCol++; } traverseRow++; }
		 */
		return false;
	}

}