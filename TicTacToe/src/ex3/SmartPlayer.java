/**
 * 
 */
package ex3;

// TODO: Auto-generated Javadoc
/**
 * The Class SmartPlayer.
 *
 * @author arebe
 */
public class SmartPlayer extends BlockingPlayer {

	/**
	 * Instantiates a new smart player.
	 *
	 * @param n
	 *            the n
	 * @param m
	 *            the m
	 */
	public SmartPlayer(String n, char m) {
		super(n, m);
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ex3.BlockingPlayer#makeMove()
	 */
	public void makeMove() {
		// check for winning
		for (int r = 0; r <= 2; r++) {
			for (int c = 0; c <= 2; c++) {
				if ((board.getMark(r, c) == Constants.SPACE_CHAR) && testForWinning(r, c)) {
					// madeMark = true;
					board.addMark(r, c, mark);
					return;
				}
			}
		}

		for (int r = 0; r <= 2; r++) {
			for (int c = 0; c <= 2; c++) {
				if ((board.getMark(r, c) == Constants.SPACE_CHAR) && testForBlocking(r, c)) {
					board.addMark(r, c, mark);
					// madeMark = true;
					return;
				}
			}
		}

		// play randomly
		RandomGenerator newNumber = new RandomGenerator();
		int col, row;

		do {
			col = newNumber.discrete(0, 2);
			row = newNumber.discrete(0, 2);
		} while (board.getMark(row, col) != Constants.SPACE_CHAR);

		board.addMark(row, col, mark);

	}

	/**
	 * Test for winning.
	 *
	 * @param row
	 *            the row
	 * @param col
	 *            the col
	 * @return true, if successful
	 */
	public boolean testForWinning(int row, int col) {
		boolean check = checkRow(row, col);
		if (check == true) {
			return true;
		}

		check = checkCol(row, col);
		if (check == true) {
			return true;
		}

		check = checkDiagonal(row, col);
		if (check == true) {
			return true;
		}

		return false;

	}

	/**
	 * Check row.
	 *
	 * @param row
	 *            the row
	 * @param col
	 *            the col
	 * @return true, if successful
	 */
	private boolean checkRow(int row, int col) {
		boolean check = true;

		for (int c = 0; c <= 2; c++) {
			if (c != col && board.getMark(row, c) != mark) {
				check = false;
			}
		}
		return check;
	}

	/**
	 * Check col.
	 *
	 * @param row
	 *            the row
	 * @param col
	 *            the col
	 * @return true, if successful
	 */
	private boolean checkCol(int row, int col) {
		boolean check = true;

		for (int r = 0; r <= 2; r++) {
			if (r != row && board.getMark(r, col) != mark) {
				check = false;
			}
		}
		return check;
	}

	/**
	 * Check diagonal.
	 *
	 * @param row
	 *            the row
	 * @param col
	 *            the col
	 * @return true, if successful
	 */
	private boolean checkDiagonal(int row, int col) {
		boolean check = false;
		// Top Left
		if (row == 0 && col == 0) {
			if ((board.getMark(1, 1) == mark) && (board.getMark(2, 2) == mark)) {
				check = true;
			}
		}
		// Top Right
		else if (row == 0 && col == 2) {
			if ((board.getMark(1, 1) == mark) && (board.getMark(2, 0) == mark)) {
				check = true;
			}
		}
		// Bottom Left
		else if (row == 2 && col == 0) {
			if ((board.getMark(1, 1) == mark) && (board.getMark(0, 2) == mark)) {
				check = true;
			}
		}
		// Bottom Right
		else if (row == 2 && col == 2) {
			if ((board.getMark(1, 1) == mark) && (board.getMark(0, 0) == mark)) {
				check = true;
			}
		}
		// Center
		else if (row == 1 && col == 1) {
			if ((board.getMark(2, 2) == mark) && (board.getMark(0, 0) == mark)) {
				check = true;
			} else if ((board.getMark(2, 0) == mark) && (board.getMark(0, 2) == mark)) {
				check = true;
			}
		}

		return check;
	}
}