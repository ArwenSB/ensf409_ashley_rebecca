package ex3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

// TODO: Auto-generated Javadoc
/**
 * Contains the data about a player in the game.
 * 
 * @author Ashley
 * @version 1.0
 * @since January 31, 2018
 */
abstract public class Player {

	/** The name of the player. */
	protected String name;

	/** The current state of the board. */
	protected Board board;

	/** The data about the opposing player. */
	protected Player opponent;

	/** The mark being used by the player. */
	protected char mark;

	/**
	 * Constructs the player using the data given.
	 *
	 * @param n
	 *            the name of the player
	 * @param m
	 *            the mark being used by the player
	 */
	public Player(String n, char m) {
		name = n;
		mark = m;
	}

	/**
	 * Runs one turn of the game.
	 */
	abstract public void play();

	/**
	 * Allows the player to add a new mark to the board.
	 */
	abstract public void makeMove();

	/**
	 * reads user input from console.
	 * 
	 * @return the user input
	 */
	public String input() {
		BufferedReader stdin;
		stdin = new BufferedReader(new InputStreamReader(System.in));
		String in;
		try {
			in = stdin.readLine();
		} catch (IOException e) {
			System.out.println("IO Error");
			in = null;
		}
		return in;
	}

	/**
	 * Sets the opponent.
	 *
	 * @param Opponent
	 *            the new opponent
	 */
	public void setOpponent(Player Opponent) {
		opponent = Opponent;
	}

	/**
	 * Sets the board.
	 *
	 * @param theBoard
	 *            the new board
	 */
	public void setBoard(Board theBoard) {
		board = theBoard;
	}

}
