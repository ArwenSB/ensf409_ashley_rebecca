/**
 * 
 */
package ex3;

// TODO: Auto-generated Javadoc
/**
 * The Class HumanPlayer.
 *
 * @author arebe
 */
public class HumanPlayer extends Player {

	/**
	 * Instantiates a new human player.
	 *
	 * @param n
	 *            the n
	 * @param m
	 *            the m
	 */
	public HumanPlayer(String n, char m) {
		super(n, m);
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ex3.Player#play()
	 */
	public void play() {

		if (board.xWins())
			System.out.println("Game over: " + opponent.name + " wins!");
		else if (board.oWins())
			System.out.println("Game over : " + opponent.name + " wins!");
		else if (board.isFull())
			System.out.println("Game over: Tie!");
		else {
			System.out.println(name + "'s turn!");
			makeMove();
			board.display();
			opponent.play();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ex3.Player#makeMove()
	 */
	public void makeMove() {

		System.out.print("\nPlease enter the row you want to play in");
		String sRow = input();
		while (sRow == null) {
			System.out.print("Please try again: ");
			sRow = input();
		}
		int row = Integer.parseInt(sRow);

		System.out.print("\nPlease enter the column you want to play in");
		String sCol = input();
		while (sCol == null) {
			System.out.print("Please try again: ");
			sCol = input();
		}
		int col = Integer.parseInt(sCol);

		while (board.getMark(row, col) != Constants.SPACE_CHAR) {
			System.out.println("That space is full, try a different space!");

			System.out.print("\nPlease enter the row you want to play in");
			sRow = input();
			while (sRow == null) {
				System.out.print("Please try again: ");
				sRow = input();
			}
			row = Integer.parseInt(sRow);

			System.out.print("\nPlease enter the column you want to play in");
			sCol = input();
			while (sCol == null) {
				System.out.print("Please try again: ");
				sCol = input();
			}
			col = Integer.parseInt(sCol);
		}

		board.addMark(row, col, mark);
	}
}
