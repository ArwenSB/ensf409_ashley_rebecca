/**
 * 
 */
package ex3;

// TODO: Auto-generated Javadoc
/**
 * The Class RandomPlayer.
 *
 * @author arebe
 */
public class RandomPlayer extends Player {

	/**
	 * Instantiates a new random player.
	 *
	 * @param n
	 *            the n
	 * @param m
	 *            the m
	 */
	public RandomPlayer(String n, char m) {
		super(n, m);
		// TODO Auto-generated constructor stub
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ex3.Player#play()
	 */
	public void play() {

		if (board.xWins())
			System.out.println("Game over: " + opponent.name + " wins!");
		else if (board.oWins())
			System.out.println("Game over : " + opponent.name + " wins!");
		else if (board.isFull())
			System.out.println("Game over: Tie!");
		else {
			System.out.println(name + "'s turn!");
			makeMove();
			board.display();
			opponent.play();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see ex3.Player#makeMove()
	 */
	public void makeMove() {
		RandomGenerator newNumber = new RandomGenerator();
		int col, row;

		do {
			col = newNumber.discrete(0, 2);
			row = newNumber.discrete(0, 2);
		} while (board.getMark(row, col) != Constants.SPACE_CHAR);

		board.addMark(row, col, mark);
	}

}
