package ex3;

// TODO: Auto-generated Javadoc
/**
 * The symbol constants for the game.
 *
 * @author UofC
 * @version 1.0
 * @since January 31, 2017
 */
public interface Constants {

	/** The Constant SPACE_CHAR. */
	static final char SPACE_CHAR = ' ';

	/** The Constant LETTER_O. */
	static final char LETTER_O = 'O';

	/** The Constant LETTER_X. */
	static final char LETTER_X = 'X';
}
