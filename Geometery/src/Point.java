package ex2;

// TODO: Auto-generated Javadoc
/**
 * The Class Point.
 */
class Point{
	
	/** The colour. */
	private Colour colour;
	
	/** The y coordinate. */
	private Double xCoordinate, yCoordinate;

	
	/**
	 * Instantiates a new point.
	 *
	 * @param a the a
	 * @param b the b
	 * @param c the c
	 */
	public Point(Double a, Double b, Colour c){
		colour = (c);
		xCoordinate = a;
		yCoordinate = b;
	}
	
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
	public String toString()   {
		String s;
		s = "X-coordinate: " + xCoordinate +  "\nY-coordinate: " + yCoordinate +
				"\n" + colour + " point" ;
		return s;
	}
	
	/**
	 * Gets the x.
	 *
	 * @return the x
	 */
	public Double  getx()   {
		return xCoordinate;
	}
	
	/**
	 * Sets the x.
	 *
	 * @param newvalue the new x
	 */
	public void  setx(Double newvalue){
		xCoordinate = newvalue;
	}
	
	/**
	 * Gets the y.
	 *
	 * @return the y
	 */
	public Double  gety()   {
		return yCoordinate;
	}
	
	/**
	 * Sets the y.
	 *
	 * @param newvalue the new y
	 */
	public void  sety(Double newvalue){
		yCoordinate = newvalue;
	}
	
	/**
	 * Distance.
	 *
	 * @param other the other
	 * @return the double
	 */
	public Double  distance(Point  other){
		Double dist_x = other.xCoordinate - xCoordinate;
		Double dist_y = other.yCoordinate - yCoordinate;
		
		return (Math.sqrt(Math.pow(dist_x, 2) + Math.pow(dist_y, 2)));
	}
	
	/**
	 * Distance.
	 *
	 * @param that the that
	 * @param other the other
	 * @return the double
	 */
	public static Double  distance (Point  that, Point  other){
		Double dist_x = other.xCoordinate - that.xCoordinate;
		Double dist_y = other.yCoordinate - that.yCoordinate;
		
		return (Math.sqrt(Math.pow(dist_x, 2) + Math.pow(dist_y, 2)));
	}
}