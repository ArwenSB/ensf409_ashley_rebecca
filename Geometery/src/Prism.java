package ex2;

// TODO: Auto-generated Javadoc
/**
 * The Class Prism.
 */
class Prism extends Rectangle {
	
	/** The height. */
	private Double height;
	
	/**
	 * Instantiates a new prism.
	 *
	 * @param x the x
	 * @param y the y
	 * @param l the l
	 * @param w the w
	 * @param h the h
	 * @param name the name
	 * @param colour the colour
	 */
	public Prism(Double x, Double y, Double l, Double w, Double h, String  name, Colour colour){
		super(x, y, l, w, name, colour);
		height = h;
	}
	
	/**
	 * Sets the height.
	 *
	 * @param h the new height
	 */
	public void  set_height(Double h){
		height = h;
	}
	
	/**
	 * Height.
	 *
	 * @return the double
	 */
	public Double  height() {
		return height;
	}
	
	/* (non-Javadoc)
	 * @see ex2.Rectangle#area()
	 */
	public Double  area(){
		return  2 * (length * width) + 2 * (height * length) + 2 * (height * width); 
	}
	
	/* (non-Javadoc)
	 * @see ex2.Rectangle#perimeter()
	 */
	public Double  perimeter(){
		return  width  * 2 + length * 2;
	}
	
	/* (non-Javadoc)
	 * @see ex2.Rectangle#volume()
	 */
	public Double  volume(){
		return  width  * length * height;
	}
	
	
	/* (non-Javadoc)
	 * @see ex2.Rectangle#toString()
	 */
	public String toString(){
		String s = super.toString()+ "\nHeight: " + height;
		return s;
	}
}