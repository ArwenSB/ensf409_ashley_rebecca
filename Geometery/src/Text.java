package ex2;

// TODO: Auto-generated Javadoc
/**
 * The Class Text.
 */
public class Text {
    
    /** The text. */
    private String text;

	/**
	 * Instantiates a new text.
	 *
	 * @param text the text
	 */
	public Text(String text) {
       this.text = text;
	}
	
	
	/**
	 * Sets the text.
	 *
	 * @param newText the new text
	 */
	public void setText(String newText){
		text = newText;
	}
	
	/**
	 * Gets the text.
	 *
	 * @return the text
	 */
	public String getText(){
		return text ;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString(){
		return (text);
	}
}
