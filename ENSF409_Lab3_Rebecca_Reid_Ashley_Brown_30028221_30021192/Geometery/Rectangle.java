package ex2;

// TODO: Auto-generated Javadoc
/**
 * The Class Rectangle.
 */
public class Rectangle extends Shape {
	
	/** The length. */
	protected Double width, length;
	
	/**
	 * Instantiates a new rectangle.
	 *
	 * @param x_origin the x origin
	 * @param y_origin the y origin
	 * @param newlength the newlength
	 * @param newwidth the newwidth
	 * @param name the name
	 * @param colour the colour
	 */
	public Rectangle(Double x_origin, Double y_origin, Double newlength, Double newwidth, String  name, Colour colour){
		super(x_origin, y_origin, name, colour);
		length= newlength;
		width =newwidth;
	}
	
	/**
	 * Sets the length.
	 *
	 * @param newlength the new length
	 */
	protected void  set_length(Double newlength){
		length = newlength;
	}
	
	/**
	 * Gets the length.
	 *
	 * @return the length
	 */
	protected Double  get_length() {
		return length;
	}
	
	/* (non-Javadoc)
	 * @see ex2.Shape#area()
	 */
	protected Double  area(){
		return  width *length;
	}
	
	/* (non-Javadoc)
	 * @see ex2.Shape#perimeter()
	 */
	protected Double  perimeter(){
		return  width  * 2 + length * 2;
	}
	
	/* (non-Javadoc)
	 * @see ex2.Shape#volume()
	 */
	protected Double  volume(){
		return 0.0;
	}
	
	/* (non-Javadoc)
	 * @see ex2.Shape#toString()
	 */
	@Override
	public String toString(){
		String s = super.toString()+ "\nWidth: " + width + "\nLength: " + length;
		return s;
	}
	
}