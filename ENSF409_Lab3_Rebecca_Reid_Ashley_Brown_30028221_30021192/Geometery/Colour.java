package ex2;

// TODO: Auto-generated Javadoc
/**
 * The Class Colour.
 */
class Colour
{
    
    /** The colour. */
    private String colour;
    
	/**
	 * Instantiates a new colour.
	 *
	 * @param s the s
	 */
	public Colour(String s) {
		colour = new String(s);
	}


    /**
     * Sets the colour.
     *
     * @param newColour the new colour
     */
    public void setColour(String newColour){
    	colour = newColour;
    }
    
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString(){
		return colour;
	}

}
