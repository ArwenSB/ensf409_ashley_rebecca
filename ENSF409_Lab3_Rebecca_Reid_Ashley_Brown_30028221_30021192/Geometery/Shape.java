package ex2;

import java.util.Comparator;

// TODO: Auto-generated Javadoc
/**
 * The Class Shape.
 */
abstract class Shape implements Comparator<Shape>, Comparable<Shape> {
	
	/** The origin. */
	protected Point origin;
	
	/** The name. */
	protected Text name;
	
	/**
	 * Area.
	 *
	 * @return the double
	 */
	abstract protected Double area();
	
	/**
	 * Perimeter.
	 *
	 * @return the double
	 */
	abstract protected Double perimeter();
	
	/**
	 * Volume.
	 *
	 * @return the double
	 */
	abstract protected Double volume();
	

	
	/**
	 * Instantiates a new shape.
	 *
	 * @param x_origin the x origin
	 * @param y_origin the y origin
	 * @param name the name
	 * @param colour the colour
	 */
	protected Shape(Double x_origin, Double y_origin, String name, Colour colour){
		
		origin = new Point(x_origin,y_origin, colour);
		this.name = new Text(name);
	}


	/**
	 * Gets the origin.
	 *
	 * @return the origin
	 */
	protected Point  getOrigin()   {
		return origin;
	}
	
	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	protected String  getName()   {
		return name.getText();
	}
	 
	
	/**
	 * Distance.
	 *
	 * @param other the other
	 * @return the double
	 */
	protected  Double distance(   Shape  other){
		return origin.distance(other.origin);
	}
	
	/**
	 * Distance.
	 *
	 * @param a the a
	 * @param b the b
	 * @return the double
	 */
	protected Double  distance(Shape a, Shape  b){
		return Point.distance(a.origin, b.origin);
	}
	
	
	/**
	 * Move.
	 *
	 * @param dx the dx
	 * @param dy the dy
	 */
	protected void  move(Double dx, Double dy){
		origin.setx(origin.getx()+dx);
		origin.sety(origin.gety()+dy);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Shape s) {
		return (this.getName()).compareTo(s.getName());
	}
	
	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare(Shape s1, Shape s2) {
		return (s1.getName()).compareTo(s2.getName());
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString(){
		String s = "\nShape name: " + name + "\nOrigin: " + origin;
		return s;
	}

}

