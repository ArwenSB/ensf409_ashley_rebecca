package ex3;

// TODO: Auto-generated Javadoc
/**
 * The Class Referee.
 *
 * @author Ashley
 * @version 1.0
 * @since January 31, 2018
 */
public class Referee {

	/** The data about the player playing 'x'. */
	private Player xPlayer;

	/** The data about the player playing 'o'. */
	private Player oPlayer;

	/** The data about the current state of the board. */
	private Board board;

	/**
	 * Default constructor of referee.
	 */
	public Referee() {
	}

	/**
	 * Run the game.
	 */
	public void runTheGame() {
		xPlayer.setOpponent(oPlayer);
		oPlayer.setOpponent(xPlayer);

		board.display();
		xPlayer.play();
	}

	/**
	 * Assigns a new board to the referee.
	 * 
	 * @param board
	 *            the board replacing the current one.
	 */
	public void setBoard(Board board) {
		this.board = board;
	}

	/**
	 * Assigns a new o player.
	 *
	 * @param oPlayer
	 *            the data about player o
	 */
	public void setoPlayer(Player oPlayer) {
		this.oPlayer = oPlayer;
	}

	/**
	 * Assigns a new x player.
	 *
	 * @param xPlayer
	 *            the data about player x
	 */
	public void setxPlayer(Player xPlayer) {
		this.xPlayer = xPlayer;
	}
}
