package ex3;

// TODO: Auto-generated Javadoc
//STUDENTS SHOULD ADD CLASS COMMENTS, METHOD COMMENTS, FIELD COMMENTS 

/**
 * Creates and keeps track of the current state of the board.
 *
 * @author U of C
 * @version 1.0
 * @since January 31, 2018
 */
public class Board implements Constants {

	/** A matrix of the marks on each square of the board. */
	private char theBoard[][];

	/** A counter of how many marks have been made so far in the game. */
	private int markCount;

	/**
	 * Constructs a new board object with default values -Creates a 3x3 board filled
	 * with spaces. -Sets markCount to 0.
	 */
	public Board() {
		markCount = 0;
		theBoard = new char[3][];
		for (int i = 0; i < 3; i++) {
			theBoard[i] = new char[3];
			for (int j = 0; j < 3; j++)
				theBoard[i][j] = SPACE_CHAR;
		}
	}

	/**
	 * Gets the mark.
	 *
	 * @param row
	 *            the row number
	 * @param col
	 *            the column number
	 * @return the contents of that square of the board ('X', 'O', or ' ')
	 */
	public char getMark(int row, int col) {
		return theBoard[row][col];
	}

	/**
	 * Checks if the board is full.
	 * 
	 * @return true if markCount is 9, otherwise returns false
	 */
	public boolean isFull() {
		return markCount == 9;
	}

	/**
	 * Checks if player x has won the game.
	 * 
	 * @return true if player x wins the game, otherwise returns false
	 */
	public boolean xWins() {
		if (checkWinner(LETTER_X) == 1)
			return true;
		else
			return false;
	}

	/**
	 * Checks if player o has won the game.
	 * 
	 * @return true if player o wins the game, otherwise returns false
	 */
	public boolean oWins() {
		if (checkWinner(LETTER_O) == 1)
			return true;
		else
			return false;
	}

	/**
	 * Displays a visual of the current state of the game to the console.
	 */
	public void display() {
		displayColumnHeaders();
		addHyphens();
		for (int row = 0; row < 3; row++) {
			addSpaces();
			System.out.print("    row " + row + ' ');
			for (int col = 0; col < 3; col++)
				System.out.print("|  " + getMark(row, col) + "  ");
			System.out.println("|");
			addSpaces();
			addHyphens();
		}
	}

	/**
	 * Adds a new mark to the board and increases the markCount.
	 *
	 * @param row
	 *            the row on the board to be marked
	 * @param col
	 *            the column on the board to be marked
	 * @param mark
	 *            the symbol to be added to the board
	 */
	public void addMark(int row, int col, char mark) {

		theBoard[row][col] = mark;
		markCount++;
	}

	/**
	 * returns the Board object to the default state -Fills the 3x3 board with
	 * spaces. -Sets markCount to 0.
	 */
	public void clear() {
		for (int i = 0; i < 3; i++)
			for (int j = 0; j < 3; j++)
				theBoard[i][j] = SPACE_CHAR;
		markCount = 0;
	}

	/**
	 * Checks if a specified mark has won the game.
	 *
	 * @param mark
	 *            the player symbol being checked if they won
	 * @return 1 if the mark has filled a row, column, or diagonal, otherwise
	 *         returns 0
	 */
	int checkWinner(char mark) {
		int row, col;
		int result = 0;

		for (row = 0; result == 0 && row < 3; row++) {
			int row_result = 1;
			for (col = 0; row_result == 1 && col < 3; col++)
				if (theBoard[row][col] != mark)
					row_result = 0;
			if (row_result != 0)
				result = 1;
		}

		for (col = 0; result == 0 && col < 3; col++) {
			int col_result = 1;
			for (row = 0; col_result != 0 && row < 3; row++)
				if (theBoard[row][col] != mark)
					col_result = 0;
			if (col_result != 0)
				result = 1;
		}

		if (result == 0) {
			int diag1Result = 1;
			for (row = 0; diag1Result != 0 && row < 3; row++)
				if (theBoard[row][row] != mark)
					diag1Result = 0;
			if (diag1Result != 0)
				result = 1;
		}
		if (result == 0) {
			int diag2Result = 1;
			for (row = 0; diag2Result != 0 && row < 3; row++)
				if (theBoard[row][3 - 1 - row] != mark)
					diag2Result = 0;
			if (diag2Result != 0)
				result = 1;
		}
		return result;
	}

	/**
	 * Displays the column headers of the board.
	 */
	void displayColumnHeaders() {
		System.out.print("          ");
		for (int j = 0; j < 3; j++)
			System.out.print("|col " + j);
		System.out.println();
	}

	/**
	 * Displays the horizontal lines of the board.
	 */
	void addHyphens() {
		System.out.print("          ");
		for (int j = 0; j < 3; j++)
			System.out.print("+-----");
		System.out.println("+");
	}

	/**
	 * Displays the horizontal spaces of the board.
	 */
	void addSpaces() {
		System.out.print("          ");
		for (int j = 0; j < 3; j++)
			System.out.print("|     ");
		System.out.println("|");
	}
}
